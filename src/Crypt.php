<?php
namespace lucan\openeyu;
use phpseclib\Crypt\RSA;

class Crypt
{
     public function __construct()
    {
        $this->rsa = new RSA();
    }

    public function rsa($string, $encrypt=true)
    {
        try {
            if ($encrypt) {
                $this->rsa->loadKey($this->getRsaPrivateKey());
                $this->rsa->setEncryptionMode(RSA::ENCRYPTION_NONE);

                return base64_encode($this->rsa->encrypt(trim($string)));
            } else {
                $this->rsa->loadKey($this->getRsaPublicKey());
                $this->rsa->setEncryptionMode(RSA::ENCRYPTION_NONE);
                return trim($this->rsa->decrypt(base64_decode($string)));
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }

    public function setRsaPrivateKey($privateKey)
    {
        if (empty($privateKey)) {
            return false;
        }
        $this->rsaKey['privateKey'] = $privateKey;
    }

    public function setRsaPublicKey($publicKey)
    {
        if (empty($publicKey)) {
            return false;
        }
        $this->rsaKey['publicKey'] = $publicKey;
    }

    public function getRsaPrivateKey()
    {
        if (empty($this->rsaKey['privateKey'])) {
            throw new \Exception('rsa key is empty: setRsaPrivateKey()',401);
        }
        return $this->rsaKey['privateKey'];
    }

    public function getRsaPublicKey()
    {
        if (empty($this->rsaKey['publicKey'])) {
            new \Exception('rsa key is empty: setRsaPublicKey()',401);
        }
        return $this->rsaKey['publicKey'];
    }

}